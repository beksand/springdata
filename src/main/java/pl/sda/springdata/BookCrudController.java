package pl.sda.springdata;

import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.springdata.dao.BookDAO;
import pl.sda.springdata.dao.BookDAOImpl;
import pl.sda.springdata.entities.Book;

import javax.transaction.Transactional;
import java.util.List;

@org.springframework.stereotype.Controller
public class BookCrudController {
    @Autowired
    private BookDAO bookDAO;


    @RequestMapping(value = "/book/add/{title:[A-Za-z0-9]+}/{author:[A-Za-z0-9]+}", method = RequestMethod.GET)
    public ModelAndView saveBook(@PathVariable("title") String title, @PathVariable("author") String author){
        ModelAndView modelAndView = new ModelAndView("book");
        modelAndView.addObject("title", title);
        modelAndView.addObject("author", author);
        Book book = new Book();
        book.setTitle(title);
        book.setAuthor(author);
        bookDAO.save(book);

        return modelAndView;

    }
    @RequestMapping(value = "/book/view/{id:[\\d]+}", method = RequestMethod.GET)
    @Transactional
    public ModelAndView loadBook(@PathVariable("id") Long id){
        Book book = bookDAO.load(id);
        ModelAndView modelAndView = new ModelAndView("book");
        modelAndView.addObject("title", book.getTitle());
        modelAndView.addObject("author", book.getAuthor());
        return modelAndView;
    }
    @RequestMapping(value = "/book/all", method = RequestMethod.GET)

    public ModelAndView loadBooks(){
        List<Book> books = bookDAO.getAllBook();
        ModelAndView modelAndView = new ModelAndView("books");
        modelAndView.addObject("books", books);
        return modelAndView;
    }
    @RequestMapping(value = "/book/delete/{id:[\\d]+}")
    @Transactional
    public ModelAndView delete(@PathVariable("id") Long id) {
        bookDAO.delete(id);
        ModelAndView message = new ModelAndView("message");
        message.addObject("title", "book deleted");
        message.addObject("message", "the success is successful");
        return message;
    }
}
