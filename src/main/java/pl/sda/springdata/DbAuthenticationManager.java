package pl.sda.springdata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import pl.sda.springdata.dao.UsersDAO;
import pl.sda.springdata.entities.Users;

import java.util.ArrayList;
import java.util.List;

public class DbAuthenticationManager implements AuthenticationManager{

    @Autowired
    private UserService userService;
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String login = authentication.getName();
        String credentials = (String)authentication.getCredentials();
        Users user = userService.loginUser(login, credentials);
        if (user != null){
            List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
            grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));
            return new UsernamePasswordAuthenticationToken(login, credentials, grantedAuthorities);
        }
        return authentication;
    }
}
