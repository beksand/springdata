package pl.sda.springdata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import pl.sda.springdata.dao.UsersDAO;
import pl.sda.springdata.entities.Users;

import javax.transaction.Transactional;
@Component
public class UserService extends BCryptPasswordEncoder{

    @Autowired
    private UsersDAO usersDao;




    public Users loginUser(String login, String credentials) {
        Users user = usersDao.load(login);
        if (user == null){
            user.setLogin(login);
            user.setPassword(credentials);
            usersDao.save(user);
            return user;
        }

        return user;
    }
}
