package pl.sda.springdata.dao;

import pl.sda.springdata.entities.Users;

import javax.transaction.Transactional;

public interface UsersDAO {

    @Transactional
    Users save(Users user);
    @Transactional
    Users load(String login);
}
