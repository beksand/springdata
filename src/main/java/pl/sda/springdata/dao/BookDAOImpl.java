package pl.sda.springdata.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.sda.springdata.entities.Book;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Repository
public class BookDAOImpl implements BookDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public Book save(Book book) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(book);
        return book;
    }

    @Override
    @Transactional
    public Book load(Long id) {
        Session session = sessionFactory.getCurrentSession();

        return session.load(Book.class, id);
    }

    @Override
    public List<Book> getAllBook() {
        Session session = sessionFactory.getCurrentSession();
        List<Book> bookList = session.createQuery("from Book").getResultList();


        return bookList;
    }

    @Override
    public void delete(Long id) {
        Session session = sessionFactory.getCurrentSession();
        Book book= load(id);
        session.delete(book);
    }
}
