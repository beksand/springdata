package pl.sda.springdata.dao;

import pl.sda.springdata.entities.Book;

import javax.transaction.Transactional;
import java.util.List;

public interface BookDAO {
    @Transactional
    Book save(Book book);
    @Transactional
    Book load(Long id);
    @Transactional
    List<Book> getAllBook();
    @Transactional
    void delete(Long id);
}
