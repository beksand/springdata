package pl.sda.springdata.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.sda.springdata.entities.Users;

import javax.transaction.Transactional;

@Repository
public class UsersDAOImpl implements UsersDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public Users save(Users user) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(user);
        return user;
    }

    @Override
    @Transactional
    public Users load(String login) {
        Session session = sessionFactory.getCurrentSession();

        return session.load(Users.class, login);
    }
}
